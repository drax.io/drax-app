# Drax-app

Client facing app to host Drax servers.

## Development:

### Code generation:

We use [OpenAPI Generator](https://github.com/OpenAPITools/openapi-generator) to generate a dart pub used for API calls to DraxApp webservice
It's generated based on the openapi spec present in drax-app.api.yml. To generate again, run ./gen_api.sh

## Flow:

Splash Screen : Create or read device secret, determine if first time user, etc.
    -> FTU
        -> Attempt to automatically login via platform (Google, Apple)
            -> Logged in! -> Tutorial Screen -> Home Page
            || Login Screen
                -> Logged in! -> Tutorial Screen -> Home Page
    || RTU -> Home Page


## Ideas regarding in app purchases and how to bypass the 15% / 30% fee:
- In all scenarios, we should advertise installing the PWA instead of the app

- The app can have ads, to encourage users to use the PWA instead if they are aware of it OR
- CHEAT: The app can cheat the system by having webview based purchase but only after app review is complete OR (may be accomplished by feature flag or code push, or by only showing the purchase options after several days of usage) OR
- GREY AREA: The app can be a companion app with no option to purchase the service, but should link to the main website when credits are running out OR
-- Cons: User flow may be messy, may discourage users from purchasing
- The app can not have the option to purchase at all, and should be just a companion app
-- Cons: May discourage users from actually purchasing, if they dont know the website

## TODO:

- flutter_secure_storage library being used has some extra configuration required for android and linux, see details
https://pub.dev/documentation/flutter_secure_storage/latest/

- android manifest needs internet permissions https://flutter.dev/docs/cookbook/networking/fetch-data

- Add MQTT logging support https://pub.dev/packages/flutter_logs

- Add SafeArea
