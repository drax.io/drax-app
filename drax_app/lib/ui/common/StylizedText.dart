import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';

TextStyle draxTextStyle(double fontSize) => TextStyle(shadows: [
      Shadow(offset: Offset.fromDirection(0, 2), color: Colors.black),
      Shadow(offset: Offset.fromDirection(3.14 * 0.5, 2), color: Colors.black),
      Shadow(offset: Offset.fromDirection(3.14 * 1.5, 2), color: Colors.black),
      Shadow(offset: Offset.fromDirection(3.14, 2), color: Colors.black),
      Shadow(offset: Offset.fromDirection(3.14 * 0.25, 2), color: Colors.black),
      Shadow(offset: Offset.fromDirection(3.14 * 0.75, 2), color: Colors.black),
      Shadow(offset: Offset.fromDirection(3.14 * 1.25, 2), color: Colors.black),
      Shadow(offset: Offset.fromDirection(3.14 * 1.75, 2), color: Colors.black)
    ], fontFamily: "PatrickHand", color: Colors.white, fontSize: fontSize);

class StylizedTextGroup {
  TextStyle style;
  double minFontSize;
  AutoSizeGroup group = AutoSizeGroup();
  StylizedTextGroup(this.style, {this.minFontSize = 0});
  Widget text(String data) {
    return AutoSizeText(
      data,
      group: group,
      style: style,
      minFontSize: this.minFontSize,
    );
  }
}

class StylizedTextGroups {
  StylizedTextGroups._();
  static StylizedTextGroup connectionInfo =
      StylizedTextGroup(draxTextStyle(24));
  static StylizedTextGroup worldPaneButtons =
      StylizedTextGroup(draxTextStyle(38), minFontSize: 17.0);
  static StylizedTextGroup addOnList = StylizedTextGroup(draxTextStyle(20));
  static StylizedTextGroup creditPaneButtons =
      StylizedTextGroup(draxTextStyle(38), minFontSize: 17.0);
  static StylizedTextGroup freeTextButtons =
      StylizedTextGroup(draxTextStyle(38));
}
