import 'package:flutter/material.dart';

class PageWithBG extends StatelessWidget {
  final Widget child;
  PageWithBG(this.child);
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image(
          //TODO: change this image
          image: AssetImage("assets/maybe-copyrighted-image.jpg"),
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
        ),
        child,
      ],
    );
  }
}
