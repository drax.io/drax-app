import 'package:flutter/widgets.dart';
import 'Logo.dart';
import 'PageWithBG.dart';

class PageWithTitle extends StatelessWidget {
  final Widget child;
  PageWithTitle(this.child);
  Widget build(BuildContext context) {
    return PageWithBG(Center(
        child: Column(
      children: [
        Spacer(),
        Expanded(flex: 3, child: Logo()),
        Spacer(),
        Expanded(flex: 25, child: child)
      ],
    )));
  }
}
