import 'package:flutter/widgets.dart';

class Logo extends StatelessWidget {
  Logo();
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Container(
            height: 100,
            width: 350,
            child: Image(
              image: AssetImage("assets/logo_programmer_art.png"),
              alignment: Alignment.center,
            )));
  }
}
