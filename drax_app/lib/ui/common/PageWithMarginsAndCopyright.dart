import 'package:flutter/widgets.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'StylizedText.dart';
import 'PageWithTitle.dart';

class PageWithMarginsAndCopyright extends StatelessWidget {
  final Widget child;
  PageWithMarginsAndCopyright(this.child);
  Widget build(BuildContext context) {
    //TODO: remove print
    print(MediaQuery.of(context).size);
    return PageWithTitle(
      Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Flexible(
              fit: FlexFit.loose,
              child: Align(
                alignment: Alignment.topCenter,
                child: ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 750, maxHeight: 550),
                  child: child,
                ),
              )),
          Container(
              padding: EdgeInsets.all(5),
              alignment: Alignment.bottomCenter,
              child: AutoSizeText(
                "DraxApp v0.1 Copyright 2021",
                style: draxTextStyle(18),
              ))
        ],
      ),
    );
  }
}
