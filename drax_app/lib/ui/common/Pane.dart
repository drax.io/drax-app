import 'package:flutter/widgets.dart';

class Pane extends StatelessWidget {
  final Widget child;
  final String imageName;
  Pane(this.imageName, this.child);
  Widget build(BuildContext context) {
    return Container(
        child: Padding(
            padding: EdgeInsets.all(2),
            child:
                child), //Ensure child doesnt climb on top of the black border
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage(imageName),
          centerSlice: Rect.fromLTWH(8, 8, 8, 8),
        )));
  }
}

class PaddedPane extends StatelessWidget {
  final Widget child;
  final String imageName;
  PaddedPane(this.imageName, this.child);
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Pane(imageName, child),
    );
  }
}
