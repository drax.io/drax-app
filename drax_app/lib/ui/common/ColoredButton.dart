import 'package:flutter/material.dart';
import 'Pane.dart';

class ColoredButton extends StatelessWidget {
  final Widget child;
  final VoidCallback? onPressed;
  final String paneImage;
  ColoredButton(this.paneImage, this.child, {this.onPressed});
  ColoredButton.green(this.child, {this.onPressed})
      : this.paneImage = "assets/button_free_credits9_8x8x9.png";
  ColoredButton.blue(this.child, {this.onPressed})
      : this.paneImage = "assets/button9_8x8x9.png";
  ColoredButton.golden(this.child, {this.onPressed})
      : this.paneImage = "assets/button_buy_credits9_8x8x9.png";
  ColoredButton.purple(this.child, {this.onPressed})
      : this.paneImage = "assets/button_get_addons9_8x8x9.png";
  Widget build(BuildContext context) {
    return Pane(
        paneImage,
        TextButton(
            style: TextButton.styleFrom(
                padding: EdgeInsets.zero, minimumSize: Size.fromHeight(17)),
            onPressed: onPressed,
            child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(5),
                child: child)));
  }
}
