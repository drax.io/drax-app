import 'package:flutter_web_auth/flutter_web_auth.dart';
import 'package:terraria_hosting/core/Configuration.dart';
import 'package:terraria_hosting/core/api.dart';
import 'package:url_launcher/url_launcher.dart';
import '../core/utils.dart';
import 'package:sign_button/sign_button.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:terraria_hosting/ui/SplashScreen.dart';
import 'package:terraria_hosting/ui/common/StylizedText.dart';

final logger = Logger();

//PRAGMA: Cubit

class LoginCubit extends Cubit<LoginState> {
  final Configuration configuration;
  AuthAPI authAPI;
  LoginCubit({required this.configuration})
      : this.authAPI =
            AuthAPI(redirectBasePath: configuration.oAuthRedirectBasePath()),
        super(LoginStateUnknown.singleton);

  void onRoute() async {
    if (this.state is LoginStateUnknown) {
      var draxAPI = await configuration.draxAppAPI();
      if (await draxAPI.isLoggedIn()) {
        this.emit(LoginStateLoggedIn.singleton);
      } else {
        this.emit(LoginStateLoggedOut.singleton);
      }
    }
  }

  void attemptFacebookLogin() async {
    final sessionKey = await configuration.keystore().readOrCreateSecretKey();
    final authURL =
        await this.authAPI.getFacebookOAuthURL(sessionKey: sessionKey);

    this.launchOAuthURL(authURL);

    //After the user authenticates on the URL above, they will be redirected to redirectURL
    //The webpage at redirectURL will close automatically. The untested hypothesis is that this will bring us back to the app.
    //If it does not, then maybe the server needs to redirect us to an app registered url
    //TODO: Test if we come back to the app after user logs in
  }

  void launchOAuthURL(Uri url) async {
    Platform platform = getPlatform();
    bool isMobilePlatform = {Platform.ios, Platform.android}.contains(platform);
    logger.i(
        "Launching OAuth URL $url for platform $platform (isMobilePlatform=$isMobilePlatform)");
    if (isMobilePlatform) {
      await FlutterWebAuth.authenticate(
          url: url.toString(), callbackUrlScheme: "");
    } else {
      if (await canLaunch(url.toString())) {
        await launch(url.toString());
      } else {
        logger.e("Could not launch URL $url");
      }
    }
  }
}

//PRAGMA: States

abstract class LoginState {
  const LoginState();
}

class LoginStateUnknown extends LoginState {
  const LoginStateUnknown._();
  static final singleton = LoginStateUnknown._();
  @override
  bool operator ==(Object o) {
    return identical(this, o) || o is LoginStateUnknown;
  }

  int get hashCode => "Type::\$\$LoginStateUnknown".hashCode;
}

class LoginStateLoggedOut extends LoginState {
  const LoginStateLoggedOut._();
  static final singleton = LoginStateLoggedOut._();
  @override
  bool operator ==(Object o) {
    return identical(this, o) || o is LoginStateLoggedOut;
  }

  int get hashCode => "Type::\$\$LoginStateLoggedOut".hashCode;
}

class LoginStateLoggedIn extends LoginState {
  const LoginStateLoggedIn._();
  static final singleton = LoginStateLoggedIn._();
  @override
  bool operator ==(Object o) {
    return identical(this, o) || o is LoginStateLoggedIn;
  }

  int get hashCode => "Type::\$\$LoginStateLoggedIn".hashCode;
}

class LoginStateFacebook extends LoginState {
  const LoginStateFacebook._();
  static final singleton = LoginStateFacebook._();
  @override
  bool operator ==(Object o) {
    return identical(this, o) || o is LoginStateFacebook;
  }

  int get hashCode => "Type::\$\$LoginStateFacebook".hashCode;
}

class LoginStateGoogle extends LoginState {
  const LoginStateGoogle._();
  static final singleton = LoginStateGoogle._();
  @override
  bool operator ==(Object o) {
    return identical(this, o) || o is LoginStateGoogle;
  }

  int get hashCode => "Type::\$\$LoginStateGoogle".hashCode;
}

class LoginStateApple extends LoginState {
  const LoginStateApple._();
  static final singleton = LoginStateApple._();
  @override
  bool operator ==(Object o) {
    return identical(this, o) || o is LoginStateApple;
  }

  int get hashCode => "Type::\$\$LoginStateApple".hashCode;
}

//Widgets:

class LoginScreenWidget extends StatelessWidget {
  final LoginCubit loginCubit;
  const LoginScreenWidget({required this.loginCubit});
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: this.loginCubit,
        builder: (context, state) {
          final Widget child;
          if (state is LoginStateUnknown) {
            child =
                SplashScreenWidget(SpinKitFadingCircle(color: Colors.white));
          } else if (state is LoginStateLoggedIn) {
            child = SplashScreenWidget(
                Text("Successfuly logged in.", style: draxTextStyle(34)));
          } else if (state is LoginStateLoggedOut) {
            child = LoginOptionsWidget(loginCubit: this.loginCubit);
          } else {
            child = SplashScreenWidget(Text("Unexpected error during login.",
                style: draxTextStyle(34)));
          }
          return child;
        });
  }
}

class LoginOptionsWidget extends StatelessWidget {
  final LoginCubit loginCubit;
  const LoginOptionsWidget({required this.loginCubit});
  Widget build(BuildContext context) {
    return SplashScreenWidget(Column(
      children: [
        SignInButton(
            buttonType: ButtonType.facebook,
            onPressed: () {
              loginCubit.attemptFacebookLogin();
            }),
      ],
    ));
  }
}
