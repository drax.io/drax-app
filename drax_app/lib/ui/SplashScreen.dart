import 'package:flutter/material.dart';
import 'common/Logo.dart';
import 'common/PageWithBG.dart';

class SplashScreenWidget extends StatelessWidget {
  final Widget child;
  const SplashScreenWidget(this.child);
  Widget build(BuildContext context) {
    return PageWithBG(Column(
      children: [
        Spacer(),
        Center(child: Logo()),
        Spacer(),
        Center(child: this.child),
        Spacer()
      ],
    ));
  }
}
