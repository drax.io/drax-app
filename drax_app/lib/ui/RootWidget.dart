import 'package:flutter/material.dart';
import 'RouterDelegate.dart';

class RootWidget extends StatelessWidget {
  final String title;
  final RootCubit rootCubit;
  final RouterDelegate<RootState> routerDelegate;
  const RootWidget(
      {required this.title,
      required this.rootCubit,
      required this.routerDelegate});

  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: AppRouteInformationParser(),
      routerDelegate: this.routerDelegate,
      title: this.title,
      theme: ThemeData(
        primarySwatch: Colors.green,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}
