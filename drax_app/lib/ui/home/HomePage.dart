import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:logger/logger.dart';
import 'package:terraria_hosting/ui/Tutorial.dart';
import '../common/StylizedText.dart';
import '../common/PageWithMarginsAndCopyright.dart';
import 'WorldPane.dart';
import 'CreditPane.dart';
import '../SplashScreen.dart';

final logger = Logger();

//PRAGMA: Cubit

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeStateSplashScreen.singleton);
  void onRoute() async {
    final didOnboard =
        (await SharedPreferences.getInstance()).getBool("onboarded");
    if (didOnboard == true) {
      logger.i("This is a returning user, skipping onboarding");
      this.emit(HomeStateOnboarded.singleton);
    } else {
      logger.i("This is a first time user, commencing onboarding");
      this.emit(HomeStateOnboarding.singleton);
    }
  }

  @override
  void onChange(Change<HomeState> change) {
    super.onChange(change);
    this.onLoadState(change.nextState);
  }

  void onLoadState(HomeState state) {
    if (state is HomeStateOnboarded) {}
  }
}

//PRAGMA: States

abstract class HomeState {
  const HomeState();
}

class HomeStateSplashScreen extends HomeState {
  const HomeStateSplashScreen._();
  static final singleton = HomeStateSplashScreen._();
  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is HomeStateSplashScreen;
  }

  int get hashCode => "Type::\$\$HomeStateSplashScreen".hashCode;
}

class HomeStateOnboarded extends HomeState {
  const HomeStateOnboarded._();
  static final singleton = HomeStateOnboarded._();
  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is HomeStateOnboarded;
  }

  int get hashCode => "Type::\$\$HomeStateOnboarded".hashCode;
}

class HomeStateOnboarding extends HomeState {
  const HomeStateOnboarding._();
  static final singleton = HomeStateOnboarding._();
  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is HomeStateOnboarding;
  }

  int get hashCode => "Type::\$\$HomeStateOnboarding".hashCode;
}

//PRAGMA: Widget

class HomePage extends StatelessWidget {
  final WorldPaneCubit worldPaneCubit;
  final CreditPaneCubit creditPaneCubit;
  const HomePage({required this.worldPaneCubit, required this.creditPaneCubit});
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      builder: (context, state) {
        if (state is HomeStateSplashScreen) {
          return SplashScreenWidget(SpinKitWave(color: Colors.white));
        } else if (state is HomeStateOnboarding) {
          return TutorialWidget();
        }

        return PageWithMarginsAndCopyright(Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Flexible(
                flex: 10,
                child: WorldPane(
                  cubit: worldPaneCubit,
                )),
            Container(height: 5),
            Flexible(
                flex: 3,
                child: CreditPane(
                  cubit: creditPaneCubit,
                )),
            Expanded(
              flex: 3,
              child: Column(
                children: [
                  Expanded(
                    child: TextButton(
                        child: StylizedTextGroups.freeTextButtons
                            .text("How To Use"),
                        onPressed: () => {print("How To Use clicked")}),
                  ),
                  Expanded(
                      child: TextButton(
                          child: StylizedTextGroups.freeTextButtons
                              .text("Settings"),
                          onPressed: () => {print("Settings clicked")})),
                ],
              ),
            )
          ],
        ));
      },
    );
  }
}
