import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../common/Pane.dart';
import '../common/StylizedText.dart';
import '../common/ColoredButton.dart';

//PRAGMA: Cubit

class CreditPaneCubit extends Cubit<CreditBalanceState> {
  CreditPaneCubit() : super(CreditBalanceStateLoading.singleton);
}

//PRAGMA: States

abstract class CreditBalanceState {
  const CreditBalanceState();
}

class CreditBalanceStateLoading extends CreditBalanceState {
  CreditBalanceStateLoading._();
  static final CreditBalanceStateLoading singleton =
      CreditBalanceStateLoading._();

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is CreditBalanceStateLoading;
  }

  int get hashCode => "Type::\$\$CreditBalanceStateLoading".hashCode;
}

class CreditBalanceStateError extends CreditBalanceState {
  final String message;
  const CreditBalanceStateError(this.message);
  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is CreditBalanceStateError && o.message == this.message;
  }

  int get hashCode => message.hashCode;
}

class CreditBalanceStateLoaded extends CreditBalanceState {
  final double credit;
  const CreditBalanceStateLoaded(this.credit);
  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is CreditBalanceStateLoaded && o.credit == this.credit;
  }

  int get hashCode => credit.hashCode;
}

//PRAGMA: Widget

class CreditPaneError extends StatelessWidget {
  final String message;
  const CreditPaneError(this.message);
  Widget build(BuildContext context) {
    return Center(
        child: AutoSizeText(
      "Something went wrong... :(\n${this.message}",
      style: draxTextStyle(30),
    ));
  }
}

class CreditPaneLoaded extends StatelessWidget {
  final CreditBalanceStateLoaded state;
  const CreditPaneLoaded(this.state);
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Flexible(
              flex: 10,
              child: Center(
                  child: AutoSizeText("Account Credits: ${state.credit}",
                      style: draxTextStyle(24)))),
          Flexible(
              flex: 14,
              child: Row(
                // crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                      child: ColoredButton.green(
                    StylizedTextGroups.creditPaneButtons.text(
                      "Get Free Credits",
                    ),
                    onPressed: () => {print("Get Free Credits clicked")},
                  )),
                  Container(
                    width: 10,
                  ),
                  Expanded(
                      child: ColoredButton.golden(
                    StylizedTextGroups.creditPaneButtons.text(
                      "Buy Credits",
                    ),
                    onPressed: () => {print("Buy Credits clicked")},
                  ))
                ],
              )),
        ]);
  }
}

class CreditPane extends StatelessWidget {
  final CreditPaneCubit cubit;
  const CreditPane({required this.cubit});
  Widget build(BuildContext context) {
    return PaddedPane(
        "assets/credit9_8x8x9.png",
        Padding(
            padding: EdgeInsets.only(left: 5, right: 5, bottom: 5),
            child: BlocBuilder<CreditPaneCubit, CreditBalanceState>(
                bloc: this.cubit,
                builder: (context, state) {
                  final Widget ret;
                  if (state is CreditBalanceStateLoading) {
                    ret = Center(
                        child: SpinKitWave(
                      color: Colors.white,
                      size: 30,
                    ));
                  } else if (state is CreditBalanceStateError) {
                    ret = CreditPaneError(state.message);
                  } else if (state is CreditBalanceStateLoaded) {
                    ret = CreditPaneLoaded(state);
                  } else {
                    ret = CreditPaneError("Unexpected CreditPaneState");
                  }
                  return ret;
                })));
  }
}
