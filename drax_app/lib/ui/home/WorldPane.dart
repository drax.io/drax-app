import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:terraria_hosting/core/api.dart';
import '../common/Pane.dart';
import '../common/ColoredButton.dart';
import '../common/StylizedText.dart';

extension on OnlineStatus {
  String translatedString() {
    final String str;
    switch (this) {
      case OnlineStatus.online:
        {
          str = "Online"; //TODO: Translation
        }
        break;
      case OnlineStatus.offline:
      default:
        {
          str = "Offline"; //TODO: Translation
        }
        break;
    }
    return str;
  }
}

//PRAGMA: Cubit

class WorldPaneCubit extends Cubit<WorldPaneState> {
  WorldPaneCubit() : super(WorldPaneStateEmpty.singleton);
}

//PRAGMA: States

abstract class WorldPaneState {
  const WorldPaneState();
}

class WorldPaneStateEmpty extends WorldPaneState {
  WorldPaneStateEmpty._();
  static final WorldPaneStateEmpty singleton = WorldPaneStateEmpty._();
  @override
  bool operator ==(Object o) {
    return identical(this, o) || o is WorldPaneStateEmpty;
  }

  int get hashCode => "Type::\$\$WorldPaneStateEmpty".hashCode;
}

class WorldPaneStateLoading extends WorldPaneState {
  final String name;
  const WorldPaneStateLoading({
    required this.name,
  });

  @override
  bool operator ==(Object o) {
    return identical(this, o) ||
        o is WorldPaneStateLoading && o.name == this.name;
  }

  int get hashCode => this.name.hashCode;
}

class WorldPaneStateError extends WorldPaneState {
  final String message;
  const WorldPaneStateError(this.message);

  @override
  bool operator ==(Object o) {
    return identical(this, o) ||
        o is WorldPaneStateLoading && o.name == this.message;
  }

  int get hashCode => this.message.hashCode;
}

class WorldPaneStateLoaded extends WorldPaneState {
  final String name;
  final String ip;
  final int port;
  final String password;
  final OnlineStatus onlineStatus;
  final int playerCount;
  final Map<String, bool> addOns;
  const WorldPaneStateLoaded({
    required this.name,
    required this.ip,
    required this.port,
    required this.password,
    required this.onlineStatus,
    required this.playerCount,
    required this.addOns,
  });
  @override
  bool operator ==(Object o) {
    return identical(this, o) ||
        o is WorldPaneStateLoaded &&
            o.name == this.name &&
            o.ip == this.ip &&
            o.port == this.port &&
            o.password == this.password &&
            o.onlineStatus == this.onlineStatus &&
            o.playerCount == this.playerCount &&
            mapEquals(o.addOns, this.addOns);
  }

  int get hashCode =>
      this.name.hashCode +
      this.ip.hashCode +
      this.port.hashCode +
      this.password.hashCode +
      this.onlineStatus.hashCode +
      this.addOns.hashCode;
}

//PRAGMA: Widgets

class WorldPane extends StatelessWidget {
  final WorldPaneCubit cubit;
  const WorldPane({required this.cubit});
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: this.cubit,
        builder: (context, state) {
          final Widget mainPaneWidget;
          String? worldName;
          if (state is WorldPaneStateLoaded) {
            worldName = state.name;
            mainPaneWidget = _WorldPaneHelper(state);
          } else if (state is WorldPaneStateEmpty) {
            mainPaneWidget = Placeholder(); //TODO: Make a world creation button
          } else if (state is WorldPaneStateLoading) {
            worldName = state.name;
            mainPaneWidget =
                Center(child: SpinKitChasingDots(color: Colors.white));
          } else if (state is WorldPaneStateError) {
            mainPaneWidget = _WorldPaneError(state.message);
          } else {
            mainPaneWidget = _WorldPaneError("Unexpected WorldPaneState");
          }
          return Stack(fit: StackFit.expand, children: [
            Positioned.fill(
                child: Container(
                    padding: EdgeInsets.only(top: 10),
                    child: Opacity(
                        opacity: 0.87,
                        child: PaddedPane(
                            "assets/world_pane9_8x8x9.png", mainPaneWidget)))),
            if (worldName != null)
              Container(
                  alignment: Alignment.topCenter,
                  padding: EdgeInsets.only(right: 120),
                  height: 40,
                  child: Row(children: [
                    Spacer(),
                    Expanded(flex: 5, child: _WorldSelector(worldName))
                  ])),
          ]);
        });
  }
}

class _WorldPaneError extends StatelessWidget {
  final String message;
  const _WorldPaneError(this.message);
  Widget build(BuildContext context) {
    return Center(
        child: AutoSizeText(
      "Something went wrong... :(\n${this.message}",
      style: draxTextStyle(30),
    ));
  }
}

class _WorldPaneHelper extends StatelessWidget {
  final WorldPaneStateLoaded state;
  const _WorldPaneHelper(this.state);
  Widget build(BuildContext context) {
    final String onlinePlayersString;
    final addOns = state.addOns;
    if (state.playerCount == 1) {
      onlinePlayersString =
          "${state.playerCount} player online"; //TODO: Translation
    } else {
      onlinePlayersString =
          "${state.playerCount} players online"; //TODO: Translation
    }
    return Column(mainAxisSize: MainAxisSize.max, children: [
      Row(children: [Spacer(), _OnlineStatusIndicator(state.onlineStatus)]),
      Expanded(
          flex: 5,
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: _ConnectionInfo(this.state))),
      Expanded(
          flex: 10,
          child: Padding(
              padding: EdgeInsets.only(left: 5, right: 5, bottom: 5),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Flexible(
                              flex: 1,
                              child: Center(
                                  child: AutoSizeText(onlinePlayersString,
                                      style: draxTextStyle(20)))),
                          Container(
                            height: 5,
                          ),
                          Flexible(
                              flex: 2,
                              child: ColoredButton.blue(
                                  StylizedTextGroups.worldPaneButtons
                                      .text("Share Link"),
                                  onPressed: () =>
                                      {print("Share Link clicked")})),
                          Container(
                            height: 5,
                          ),
                          Flexible(
                              flex: 2,
                              child: ColoredButton.blue(
                                  StylizedTextGroups.worldPaneButtons
                                      .text("How To Connect"),
                                  onPressed: () =>
                                      {print("How To Connect clicked")})),
                          Container(
                            height: 5,
                          ),
                          Flexible(
                              flex: 2,
                              child: ColoredButton.blue(
                                  StylizedTextGroups.worldPaneButtons
                                      .text("Admin Panel"),
                                  onPressed: () =>
                                      {print("Admin Panel clicked")})),
                        ]),
                  ),
                  Container(
                    width: 5,
                  ),
                  Expanded(
                      child: _AddOnPanel(
                    addOns: addOns,
                  ))
                ],
              )))
    ]);
  }
}

class _AddOnPanel extends StatelessWidget {
  final Map<String, bool> addOns;
  const _AddOnPanel({required this.addOns});
  Widget build(BuildContext context) {
    final List<String> unlockedAddOns =
        addOns.keys.where((k) => addOns[k] == true).toList();
    return Stack(alignment: Alignment.bottomCenter, children: [
      Container(
          width: double.infinity,
          child: Padding(
              padding: EdgeInsets.only(bottom: 20, top: 5),
              child: Container(
                  decoration: BoxDecoration(color: Color(0xFF453470)),
                  child: Padding(
                      padding: EdgeInsets.only(bottom: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Flexible(
                              child: AutoSizeText(
                                  "AddOns [${unlockedAddOns.length}/${this.addOns.length}]",
                                  style: draxTextStyle(24))),
                          for (final unlockedAddOn in unlockedAddOns)
                            Flexible(
                                child: StylizedTextGroups.addOnList
                                    .text(unlockedAddOn)),
                        ],
                      ))))),
      Container(
          height: 50,
          alignment: Alignment.bottomCenter,
          padding: EdgeInsets.only(left: 10, right: 10),
          child: ColoredButton.purple(
            AutoSizeText("Get Addons", style: draxTextStyle(38)),
            onPressed: () => {print("Get Addons clicked")},
          )),
    ]);
  }
}

class _ConnectionInfo extends StatelessWidget {
  final WorldPaneStateLoaded state;
  const _ConnectionInfo(this.state);
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.max,
      children: [
        Flexible(
            child: Row(children: [
          Expanded(
              child: StylizedTextGroups.connectionInfo.text("Server IP: ")),
          Container(
            width: 5,
          ),
          Flexible(
              child: StylizedTextGroups.connectionInfo.text(this.state.ip)),
        ])),
        Flexible(
            child: Row(children: [
          Expanded(
              child: StylizedTextGroups.connectionInfo.text("Server Port: ")),
          Container(
            width: 5,
          ),
          Flexible(
              child: StylizedTextGroups.connectionInfo
                  .text(this.state.port.toString())),
        ])),
        Flexible(
            child: Row(children: [
          Expanded(
              child:
                  StylizedTextGroups.connectionInfo.text("Server Password: ")),
          Container(
            width: 5,
          ),
          Flexible(
              child:
                  StylizedTextGroups.connectionInfo.text(this.state.password))
        ])),
      ],
    );
  }
}

class _WorldSelector extends StatelessWidget {
  final String name;
  const _WorldSelector(this.name);
  Widget build(BuildContext context) {
    return Container(
        height: 40,
        child: Pane(
            "assets/world_name9_8x8x9.png",
            Row(children: [
              Expanded(
                  child: Center(
                      child:
                          AutoSizeText(this.name, style: draxTextStyle(18)))),
              _BlueVerticalSeparator(),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Image(image: AssetImage("assets/drop_down.png")))
            ])));
  }
}

class _BlueVerticalSeparator extends StatelessWidget {
  Widget build(BuildContext context) {
    return Container(
        width: 2,
        height: double.infinity,
        child: Image(
          fit: BoxFit.fill,
          image: AssetImage("assets/vertical_sep.png"),
        ));
  }
}

class _OnlineStatusIndicator extends StatelessWidget {
  final OnlineStatus onlineStatus;
  const _OnlineStatusIndicator(this.onlineStatus);
  Widget build(BuildContext context) {
    final AssetImage statusImage;
    switch (this.onlineStatus) {
      case OnlineStatus.online:
        {
          statusImage = AssetImage("assets/online_orb.png");
        }
        break;
      case OnlineStatus.offline:
      default:
        {
          statusImage = AssetImage("assets/offline_orb.png");
        }
        break;
    }
    return Row(children: [
      AutoSizeText(this.onlineStatus.translatedString(),
          style: draxTextStyle(24)),
      Padding(
          padding: EdgeInsets.only(top: 5, right: 5, left: 5),
          child: Image(image: statusImage))
    ]);
  }
}
