import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:logger/logger.dart';
import 'package:terraria_hosting/ui/LoginScreen.dart';
import 'home/WorldPane.dart';
import 'home/CreditPane.dart';
import 'home/HomePage.dart';
import 'common/StylizedText.dart';
import 'SplashScreen.dart';
import 'Tutorial.dart';
import '../core/Configuration.dart';

final logger = Logger();

//PRAGMA: Cubit

class RootCubit extends Cubit<RootState> {
  final WorldPaneCubit worldPaneCubit;
  final CreditPaneCubit creditPaneCubit;
  final HomeCubit homeCubit;
  final LoginCubit loginCubit;
  final Configuration configuration;
  RootCubit(
      {required this.configuration,
      required this.worldPaneCubit,
      required this.creditPaneCubit,
      required this.homeCubit,
      required this.loginCubit})
      : super(RootStateHome()) {
    this.onLoadState(this.state);
  }
  void onLoadState(RootState state) async {
    if (state is RootStateHome) {
      this.homeCubit.onRoute();
    } else if (state is RootStateLogin) {
      this.loginCubit.onRoute();
    }
  }

  @override
  void onChange(Change<RootState> change) {
    super.onChange(change);
    this.onLoadState(change.nextState);
  }
}

//PRAGMA: States

abstract class RootState {
  const RootState();
}

class RootStateLogin extends RootState {
  const RootStateLogin._();
  static final RootStateLogin singleton = RootStateLogin._();
  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is RootStateLogin;
  }

  int get hashCode => "Type::\$\$RootStateLogin".hashCode;
}

class RootStateTutorial extends RootState {
  const RootStateTutorial._();
  static final RootStateTutorial singleton = RootStateTutorial._();
  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is RootStateTutorial;
  }

  int get hashCode => "Type::\$\$RootStateTutorial".hashCode;
}

class RootStateHome extends RootState {
  final String? serverID;
  const RootStateHome({this.serverID});
  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is RootStateHome && this.serverID == o.serverID;
  }

  int get hashCode => this.serverID.hashCode;
}

class RootStateError extends RootState {
  final String message;
  const RootStateError({required this.message});
  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is RootStateError && this.message == o.message;
  }

  int get hashCode => this.message.hashCode;
}

//PRAGMA: Routing Widget

class AppRouteInformationParser extends RouteInformationParser<RootState> {
/*
Valid paths:

'/tutorial'
: App usage info

'/login'
: Authorize user

'/'
: The main home page

'/servers/:serverId'
: The main page, with a specific server deep linked

'/createServer'
: Server creation wizard

'/serverList'
: List, delete, create servers and select them to navigate to /servers/:serverId

'/admin/:serverId'
: Server admin panel

'/settings'
: App settings

'/walkthrough/:serverId'
: Server specific "How to connect" walkthrough cards
*/

  const AppRouteInformationParser();

  @override
  Future<RootState> parseRouteInformation(
      RouteInformation routeInformation) async {
    final uri = Uri.parse(routeInformation.location ?? "/404");
    final RootState ret;

    // Handle '/'
    if (uri.pathSegments.length == 0) {
      ret = RootStateHome();
    } else if (uri.pathSegments[0] == 'tutorial') {
      ret = RootStateTutorial.singleton;
    } else if (uri.pathSegments[0] == 'login') {
      ret = RootStateLogin.singleton;
    } else if (uri.pathSegments[0] == 'servers' &&
        uri.pathSegments.length >= 2) {
      ret = RootStateHome(serverID: uri.pathSegments[1]);
    } else if (uri.pathSegments[0] == 'createserver') {
      ret = RootStateError(
          message:
              "CreateServer is not implemented yet"); //TODO: Server creation wizard
    } else if (uri.pathSegments[0] == 'serverlist') {
      ret = RootStateError(
          message:
              "Server management is not implemented yet"); //TODO: Server list ui
    } else if (uri.pathSegments[0] == 'admin') {
      ret = RootStateError(
          message:
              "Admin page is not implemented yet"); //TODO: Server administration panel
    } else {
      ret = RootStateError(message: "404 - This page doesn't exist");
    }
    logger.i(
        "parseRouteInformation mapped ${routeInformation.location}:${routeInformation.state} to $ret");
    return ret;
  }

  @override
  RouteInformation restoreRouteInformation(RootState state) {
    logger.i("restoreRouteInformation to state $state");
    final String? currentPath;
    if (state is RootStateLogin) {
      currentPath = '/login';
    } else if (state is RootStateHome) {
      if (state.serverID != null) {
        currentPath = '/servers/${state.serverID}';
      } else {
        currentPath = '/';
      }
    } else if (state is RootStateTutorial) {
      currentPath = '/tutorial';
    } else {
      currentPath = null;
    }
    return RouteInformation(location: currentPath);
  }
}

class AppRouterDelegate extends RouterDelegate<RootState>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<RootState> {
  final GlobalKey<NavigatorState> navigatorKey;
  final RootCubit rootCubit;

  AppRouterDelegate({required this.rootCubit})
      : navigatorKey = GlobalKey<NavigatorState>();

  RootState get currentConfiguration {
    return rootCubit.state;
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer(
        bloc: this.rootCubit,
        listener: (context, state) {
          logger.i("AppRouterDelegate heard state $state");
          notifyListeners();
        },
        builder: (context, state) {
          final Widget child;
          if (state is RootStateTutorial) {
            child = TutorialWidget();
          } else if (state is RootStateHome) {
            child = HomePage(
              worldPaneCubit: rootCubit.worldPaneCubit,
              creditPaneCubit: rootCubit.creditPaneCubit,
            );
          } else if (state is RootStateLogin) {
            child = LoginScreenWidget(loginCubit: rootCubit.loginCubit);
          } else {
            child = SplashScreenWidget(AutoSizeText(
                "404 - This page doesn't exist",
                style: draxTextStyle(30)));
          }

          return Navigator(
            key: navigatorKey,
            pages: [
              MaterialPage(
                  child: Scaffold(
                body: child,
              ))
            ],
            onPopPage: (route, result) {
              if (!route.didPop(result)) {
                return false;
              }
              return true;
            },
          );
        });
  }

  @override
  Future<void> setNewRoutePath(RootState path) async {
    rootCubit.emit(path);
  }
}
