import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:logger/logger.dart';
import 'common/ColoredButton.dart';
import 'common/StylizedText.dart';
import 'common/PageWithBG.dart';

final logger = Logger();

class TutorialWidget extends StatelessWidget {
  // TODO: onComplete

  TutorialWidget();

  Widget build(BuildContext context) {
    //TODO: Implement the tutorial
    return PageWithBG(Column(
      children: [
        AutoSizeText(
          "This will be a tutorial!",
          style: draxTextStyle(50),
        ),
        ColoredButton.purple(
          AutoSizeText(
            "Understood",
            style: draxTextStyle(30),
          ),
          onPressed: () async {
            logger.i("User has finished the tutorial!");
            var sharedPref = await SharedPreferences.getInstance();
            if (!(await sharedPref.setBool("onboarded", true))) {
              logger.e(
                  "Could not save 'onboarded = true' to shared preferences!");
            }
          },
        ),
      ],
    ));
  }
}
