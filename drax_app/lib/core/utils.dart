import 'dart:io' as DartPlatform show Platform;
import 'package:flutter/foundation.dart' show kIsWeb;

enum Platform { web, android, ios }

Platform getPlatform() {
  if (kIsWeb) {
    return Platform.web;
  } else if (DartPlatform.Platform.isAndroid) {
    return Platform.android;
  } else if (DartPlatform.Platform.isIOS) {
    return Platform.ios;
  } else {
    throw "Running on unsupported platform";
  }
}
