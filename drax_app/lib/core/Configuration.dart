import 'package:terraria_hosting/core/api.dart';
import 'package:terraria_hosting/core/keystore.dart';

abstract class Configuration {
  String draxAppAPIBasePath();
  String oAuthRedirectBasePath();
  const Configuration();
  KeyStore keystore() => DefaultKeyStore();
  Future<DraxAppAPI> draxAppAPI() async {
    var authHeaders =
        DraxAppAPIAuthHeaders(await keystore().readOrCreateSecretKey());
    return DraxAppAPI(
        authHeaders: authHeaders, basePath: this.draxAppAPIBasePath());
  }
}

class ReleaseConfiguration extends Configuration {
  const ReleaseConfiguration();
  String draxAppAPIBasePath() => "https://draxapp.katoch.co/drax-app/1.0.0";
  String oAuthRedirectBasePath() => "https://draxapp.katoch.co/drax-app/1.0.0";
}

class StagingConfiguration extends Configuration {
  const StagingConfiguration();
  String draxAppAPIBasePath() => "http://harold.katoch.co:5555/drax-app/1.0.0";
  String oAuthRedirectBasePath() =>
      "http://harold.katoch.co:5555/drax-app/1.0.0";
}

class MockConfiguration extends Configuration {
  const MockConfiguration();
  String draxAppAPIBasePath() =>
      "https://virtserver.swaggerhub.com/draxapp/drax-app/1.0.0";
  String oAuthRedirectBasePath() =>
      "https://virtserver.swaggerhub.com/draxapp/drax-app/1.0.0";
}

class PipeDreamConfiguration extends Configuration {
  // https://pipedream.com/@katochanirudh/p_brCkeGO/
  const PipeDreamConfiguration();
  String draxAppAPIBasePath() =>
      "https://b8e021acd22fb41c85f134922aee8e00.m.pipedream.net/draxapp/drax-app/1.0.0";
  String oAuthRedirectBasePath() =>
      "https://b8e021acd22fb41c85f134922aee8e00.m.pipedream.net/draxapp/drax-app/1.0.0";
}

class HerokuTestConfiguration extends Configuration {
  const HerokuTestConfiguration();
  String draxAppAPIBasePath() => "https://drax-test.herokuapp.com";
  String oAuthRedirectBasePath() => "https://drax-test.herokuapp.com";
}
