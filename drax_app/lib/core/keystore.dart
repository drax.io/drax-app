//Note: This class is not thread safe against reader-writer problem
//Therefore, the first run, or subsequent calls to resetSecretKey, may result in errors
//TODO: Implement a task queue for this

import 'utils.dart';
import 'dart:math';
import 'dart:convert';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:logger/logger.dart';

final storage = new FlutterSecureStorage();
final logger = Logger();

abstract class KeyStore {
  Future<String> readOrCreateSecretKey();
  Future<void> resetSecretKey();
}

class DefaultKeyStore extends KeyStore {
  static const secretKeyId = "draxSK";
  String? cachedSecretKey;
  Future<String> readOrCreateSecretKey() async {
    final cachedSecretKey = this.cachedSecretKey;
    if (cachedSecretKey != null) {
      return cachedSecretKey;
    }
    logger.i("Entered readOrCreateSK with ski $secretKeyId");
    String? existingKey;
    Platform platform = getPlatform();
    bool isMobilePlatform = {Platform.ios, Platform.android}.contains(platform);
    if (isMobilePlatform) {
      logger.i("Platform is android or ios");
      existingKey = await storage.read(key: secretKeyId);
    } else {
      logger.i("Platform is not android/ios");
      var sharedPref = await SharedPreferences.getInstance();
      existingKey = sharedPref.getString(secretKeyId);
    }
    if (existingKey == null) {
      logger.i("SK does not exist! creating one...");
      Random secureRandomGen = Random.secure();
      String newKey = base64UrlEncode(
          List<int>.generate(30, (i) => secureRandomGen.nextInt(256)));
      logger.i("Generated SK ${newKey[0]} of length ${newKey.length}");

      if (isMobilePlatform) {
        await storage.write(key: secretKeyId, value: newKey);
      } else {
        var sharedPref = await SharedPreferences.getInstance();
        if (!(await sharedPref.setString(secretKeyId, newKey))) {
          logger.e("Could not save SK to shared preferences!");
          throw "Could not save SK to shared preferences!";
        }
      }
      existingKey = newKey;
    } else {
      logger.i("Retrived SK ${existingKey[0]} of length ${existingKey.length}");
    }
    this.cachedSecretKey = existingKey;
    return existingKey;
  }

  Future<void> resetSecretKey() async {
    this.cachedSecretKey = null;
    Platform platform = getPlatform();
    bool isMobilePlatform = {Platform.ios, Platform.android}.contains(platform);
    logger.i("Entered resetSecretKey with ski $secretKeyId");
    if (isMobilePlatform) {
      logger.i("Platform is android or ios");
      await storage.delete(key: secretKeyId);
    } else {
      logger.i("Platform is not android or ios");
      var sharedPref = await SharedPreferences.getInstance();
      if (!(await sharedPref.remove(secretKeyId))) {
        logger.e("Could not remove SK from shared preferences!");
        throw "Could not remove SK from shared preferences!";
      }
    }
    readOrCreateSecretKey();
  }
}
