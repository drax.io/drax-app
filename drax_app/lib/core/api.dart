import 'package:logger/logger.dart';
import 'package:oauth2/oauth2.dart' as oauth2;
import 'package:draxapp_api_client/draxapp_api_client.dart';

final logger = Logger();

typedef OnlineStatus = DraxWorldOnlineStatusEnum;

//PRAGMA: API

class DraxAppAPIAuthHeaders {
  String sessionKey;
  DraxAppAPIAuthHeaders(this.sessionKey);
}

class DraxAppAPI {
  final String basePath;
  final DraxappApiClient apiClient;
  DraxAppAPIAuthHeaders authHeaders;
  DraxAppAPI({required this.authHeaders, required this.basePath})
      : this.apiClient = DraxappApiClient(basePathOverride: basePath) {
    this.apiClient.dio.options.headers["authorization"] =
        "Bearer ${authHeaders.sessionKey}";
  }

  Future<Iterable<DraxWorld>> getWorlds() async {
    final response = await apiClient.getWorldApi().getWorldList();
    final worldList = response.data;
    if (worldList == null) {
      throw "null resonse data!";
    } else {
      return worldList;
    }
  }

  Future<bool> isLoggedIn() async {
    try {
      final sessionInfo = await apiClient.getSessionApi().getSession(
        validateStatus: (status) {
          if (status == null) {
            return false;
          }
          if (status >= 200 && status < 300) {
            return true;
          } else if (status >= 401 && status < 500) {
            return true;
          }
          return false;
        },
      );
      return sessionInfo.data?.userId.isNotEmpty == true;
    } catch (e) {
      logger.e("API call to get session information failed with error: $e");
      return false;
    }
  }
}

class AuthAPI {
  //OAuth redirect
  final String redirectBasePath;
  //Facebook
  static final String facebookAppID = "286140706547717";
  static final Uri facebookOAthEndpoint =
      Uri.parse("https://www.facebook.com/v11.0/dialog/oauth");
  Uri facebookRedirectURL() =>
      Uri.parse("${this.redirectBasePath}/authenticate/\$facebook");
  //Google
  static final String googleAppID = "";
  static final Uri googleOAthEndpoint = Uri.parse("");
  Uri googleRedirectURL() =>
      Uri.parse("${this.redirectBasePath}/authenticate/\$google");
  //Apple
  static final String appleAppID = "";
  static final Uri appleOAthEndpoint = Uri.parse("");
  Uri appleRedirectURL() =>
      Uri.parse("${this.redirectBasePath}/authenticate/\$apple");

  const AuthAPI({required this.redirectBasePath});

  Future<Uri> getOAuthURL({
    required Uri authorizationEndpoint,
    required Uri redirectUrl,
    required String appID,
    required String sessionKey,
  }) async {
    final grant = oauth2.AuthorizationCodeGrant(
        appID, authorizationEndpoint, Uri(),
        secret: null);

    final authorizationUrl = grant.getAuthorizationUrl(redirectUrl,
        state: "{\"session\":\"$sessionKey\"}");

    return authorizationUrl;
  }

  Future<Uri> getFacebookOAuthURL({required String sessionKey}) async {
    return this.getOAuthURL(
        authorizationEndpoint: AuthAPI.facebookOAthEndpoint,
        redirectUrl: this.facebookRedirectURL(),
        appID: AuthAPI.facebookAppID,
        sessionKey: sessionKey);
  }
}
