import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:terraria_hosting/ui/LoginScreen.dart';
import 'package:terraria_hosting/ui/RouterDelegate.dart';
import 'package:terraria_hosting/ui/home/CreditPane.dart';
import 'package:terraria_hosting/ui/home/HomePage.dart';
import 'package:terraria_hosting/ui/home/WorldPane.dart';
import 'ui/RootWidget.dart';
import 'ui/RouterDelegate.dart';
import 'core/Configuration.dart';
import 'package:logger/logger.dart';
import 'conf_local.dart';

final logger = Logger();

void main() {
  Configuration config = getCurrentConfiguration();
  config.keystore().readOrCreateSecretKey();
  runApp(
    MultiBlocProvider(
        providers: [
          BlocProvider<WorldPaneCubit>(create: (_) => WorldPaneCubit()),
          BlocProvider<CreditPaneCubit>(create: (_) => CreditPaneCubit()),
          BlocProvider<HomeCubit>(create: (_) => HomeCubit()),
          BlocProvider<LoginCubit>(
              create: (_) => LoginCubit(configuration: config))
        ],
        child: BlocProvider<RootCubit>(
            create: (context) => RootCubit(
                configuration: config,
                worldPaneCubit: BlocProvider.of<WorldPaneCubit>(context),
                creditPaneCubit: BlocProvider.of<CreditPaneCubit>(context),
                homeCubit: BlocProvider.of<HomeCubit>(context),
                loginCubit: BlocProvider.of<LoginCubit>(context)),
            child: Builder(
                builder: (context) => RootWidget(
                      title: "Drax Terraria Server Hosting",
                      rootCubit: BlocProvider.of<RootCubit>(context),
                      routerDelegate: AppRouterDelegate(
                          rootCubit: BlocProvider.of<RootCubit>(context)),
                    )))),
  );
}
