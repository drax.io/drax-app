REL_OUTPUT_DIR="draxapp_api_client"
rm -rf "$PWD/${REL_OUTPUT_DIR}" ;
docker run --user $(id -u):$(id -g) --rm -v "${PWD}:/local" openapitools/openapi-generator-cli generate -i /local/drax-app.api.yml -g dart-dio-next -o "/local/${REL_OUTPUT_DIR}" \
--config /local/openapi_generator_config.yml \
--http-user-agent "DraxApp/unknown" \
# --model-package draxapp_api_models \
# --api-package draxapp_api_api
# --invoker-package draxapp_api_invoker \
# --package-name draxapp_api_package \

cd "$PWD/${REL_OUTPUT_DIR}"
dart pub get
dart run build_runner build